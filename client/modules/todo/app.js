'use strict'
var todo = angular.module('todo', []);
todo.config(function($stateProvider) {
  $stateProvider
    .state('app.todo', {
      url: "/todo/",
      templateUrl: "modules/todo/views/list.html",
      controller: "todoListCtrl"
    });
})

/* сервис с методами для записей */
todo.factory('Item', ['$http', function($http) {
  function Item(todoData) {
    this.data = {};
    if (todoData) {
      this.setData(todoData);
    }
  };

  Item.prototype.setData = function(data) {
    this.data = data;
  }
  Item.prototype.delete = function() {
    if (!this.data.todolist_id) {
      return false;
    }
    return $http.delete("/deleteTodo/" + this.data.todolist_id, this.data);
  }
  Item.prototype.save = function() {
    if (this.data.todolist_id) {
      return $http.put("/update/" + this.data.todolist_id, this.data);
    } else {
      return $http.post("/create", this.data);
    }

  }

  return Item;
}])

todo.factory('listManager', ['$http', '$q', 'Item', function($http, $q, List) {
  var listManager = {
    _pool: {},
    _retrieveInstance: function(todoId, todoData) {
      var instance = this._pool[todoId];
      if (instance) {
        instance.setData(todoData);
      } else {
        instance = new List(todoData);
        this._pool[todoId] = instance;
      }
      return instance;
    },

    // _load: function(todoId, deferred) {
    //   var scope = this;
    //   $http.get('/todolist', todoId)
    //     .success(function(todoData) {
    //       var list = scope._retrieveInstance(todoData.user_id, todoData);
    //       deferred.resolve(list);
    //     })
    //     .error(function() {
    //       deferred.reject();
    //     })
    // },

    loadAllList: function(data) {
      var deferred = $q.defer();
      var scope = this;

      if (!data) {
        data = 'active';
      }

      $http.get('/todolist' + '?filter=' + data)
        .success(function(todoData) {
          var lists = [];
          todoData.forEach(function(todoData) {
            var list = scope._retrieveInstance(todoData.todolist_id, todoData);
            lists.push(list);
          });
          deferred.resolve(lists);
        })
        .error(function() {
          deferred.reject();
        });
      return deferred.promise;
    }
  }
  return listManager;
}])

todo.directive('panelList', function() {
  return {
    restrict: 'E',
    templateUrl: "modules/todo/views/paneloflist.html"
  }
})
