angular.module('session')
  .controller('userLog', function($scope, $http, $state) {

    $scope.form = {
      email: "",
      password: ""
    };

    //******** check empty log form?
    $scope.noempty = function() {
        var logForm = $scope.form;
        if (logForm.email !== "" && logForm.email !== undefined && logForm.password !== undefined && logForm.password !== "") {
          return false;
        } else {
          return true;
        }
      }
      //****************************
      //******** LogIn
    $scope.error = null;
    $scope.logIn = function() {
      $scope.error = null;
      $http.post('/login', $scope.form)
        .success(function(response) {
          $state.go('app.todo');
        })
        .error(function(error) {
          $scope.error = error.message;
        })
    }
  })
  .controller('singUp', function($scope, $http, $state) {
    $scope.error = false;

    $scope.sing = function() {
      $scope.error = false;
      var registerData = $scope.registerForm;
      if (registerData.password !== registerData.rePassword) {
        return $scope.error = true;
      }
      $http.post('/singup', $scope.registerForm)
        .success(function(response) {
          $state.go('app.todo');
        })
        .error(function(error) {
          $scope.error = error.message;
        })
    }
  })
