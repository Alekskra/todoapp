var express = require('express');
var app = express();
var fs = require('fs');
var bodyParser = require('body-parser');
var mysql = require('mysql');
var cookieParser = require('cookie-parser');

var uri = process.env.CLEARDB_DATABASE_URL || "mysql://root:123456@localhost:3306/todoapp";
var connection = mysql.createConnection(uri);

connection.connect();

app.use(express.static('client'));
app.use("/node_modules", express.static('node_modules'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cookieParser());

app.get('/', function(req, res) {
  var content = fs.readFileSync(__dirname + '/../client/index.html');
  res.send(content);
});

app.post('/login', function(req, res, next) {
  var params = req.body;
  var query = 'SELECT users_id, email, fullname, password FROM users WHERE email = ? AND password = ?';
  connection.query(query, [params.email, params.password], function(err, data) {
    if (err) {
      return next(err);
    }

    if (data.length !== 1) {
      return next(new Error('Invalid email or password'));
    }

    if (data[0].password !== params.password) {
      return next(new Error('Invalid email or password'));
    }

    res.cookie('user_id', data[0].users_id);

    res.json({
      email: data[0].email,
      fullname: data[0].fullname
    })
  })
})

app.post('/singup', function(req, res, next) {
  var params = req.body;
  var checkUser = 'SELECT email FROM users WHERE email = ? LIMIT 1';
  connection.query(checkUser, [params.email], function(err, data) {
    if (err) {
      return next(err);
    }
    if (data.length > 0) {
      return next(new Error("User with the same name already exists"));
    } else {
      var insert = 'INSERT INTO users (email, password, fullname) VALUES ("' + params.email + '", "' + params.password + '", "' + params.fName + '")';
      connection.query(insert, function(err, result) {
        if (err) {
          return next(err);
        }
      })
    }
  })
})

app.get('/todolist', function(req, res, next) {
  var userId = req.cookies.user_id;
  /*check user log*/
  if (!userId) {
    return next(new Error("Fuck you, not autoraize"));
  }

  var selectList = 'SELECT title, text, todolist_id, deadline FROM todolist WHERE user_id = ?';
  if (req.query.filter === "archived") {
    selectList += ' AND deleted=1';
  }
  if (req.query.filter === "active" || req.query.filter === 'overdue') {
    selectList += ' AND deleted=0';
  }

  selectList += ' LIMIT 0,10';

  connection.query(selectList, [userId], function(err, datalist) {
    if (err) {
      return next(err);
    }
    res.send(datalist);
  })
})

app.post("/create", function(req, res, next) {
  var itemData = req.body;
  var userId = req.cookies.user_id;

  if (!userId) {
    return next(new Error("Fuck you, not autoraize"));
  }
  var addQuery = 'INSERT INTO todolist (user_id, title, text, deadline) VALUES (?, ?, ?,?)';
  connection.query(addQuery, [userId, itemData.title, itemData.text, itemData.deadline], function(err, result) {
    if (err) {
      return next(err);
    }
    var selectList = 'SELECT title, text, todolist_id, deadline FROM todolist WHERE todolist_id = ?';
    connection.query(selectList, [result.insertId], function(err, datalist) {
      if (err) {
        return next(err);
      }
      res.send(datalist[0]);
    })
  })
})

app.put('/update/:todolist_id', function(req, res, next) {
  var userId = req.cookies.user_id;

  if (!userId) {
    return next(new Error("Fuck you, not autoraize"));
  }

  var addQuery = 'INSERT INTO todolist (user_id, title, text) VALUES ("' + userId + '", "' + addData.title + '", "' + addData.text + '")';
  connection.query(addQuery, function(err, result) {
    if (err) {
      return next(err);
    }
  })
})


app.delete('/deleteTodo/:todolist_id', function(req, res, next) {
  var userId = req.cookies.user_id;
  if (!userId) {
    return next(new Error("Fuck you! You are not autorize!"));
  }

  var checkTodo = 'SELECT todolist_id FROM todolist WHERE todolist_id = ?';
  connection.query(checkTodo, [req.params.todolist_id], function(err, result) {
    if (err) {
      return next(err);
    }

    if (result.length === 0) {
      return next(new Error("Fuck you, not found ToDo!!"));
    }

    var delQuery = 'DELETE FROM todolist WHERE todolist_id = ?';
    connection.query(delQuery, [req.params.todolist_id], function(err, result) {
      if (err) {
        return next(err);
      }
      res.status(204).end();
    })
  })
})

app.use(function(err, req, res, next) {
  res.status(400).json({
    message: err.message
  });
});

app.listen(process.env.PORT || 9000);
