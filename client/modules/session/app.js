angular.module('session', [])
  .config(function($stateProvider) {
    $stateProvider
      .state('app.session_log', {
        url: "/login",
        templateUrl: "/modules/session/views/login.html",
        controller: "userLog"
      })
      .state('app.session_reg', {
        url: "/singup",
        templateUrl: "/modules/session/views/register.html",
        controller: "singUp"
      })
  });
