todo.controller('todoListCtrl', ['$rootScope', '$scope', 'Item', 'listManager', function($rootScope, $scope, Item, listManager) {

  $rootScope.$on("todolistUpdate", function(event, filter) {
    listManager.loadAllList(filter).then(function(lists) {
      $scope.list = lists;
      var tst = new Date();
      var f = angular.isDate($scope.list[3].data.deadline);
      console.log(f);
      console.log($scope.list[3].data.deadline);

      console.log($scope.list);
    });

    $scope.deleteTodo = function(delData) {
      var userConfirm = confirm("You realy want delete Todo " + delData.title + " ?");
      if (userConfirm) {
        var delItem = new Item(delData);
        delItem.delete().then(function() {
          $rootScope.$emit("todolistUpdate");
        })
      }
    }
  })
  $rootScope.$emit("todolistUpdate");
}]);

todo.controller('todoCreateCtrl', ['$rootScope', '$scope', 'Item', '$filter', function($rootScope, $scope, Item, $filter) {
  $scope.addForm = {};

  //$scope.addForm.deadline = tst($scope.addForm.deadline);
  $scope.save = function() {
    console.log($scope.addForm);

    var item = new Item($scope.addForm);
    item.save().then(function() {
      $rootScope.$emit("todolistUpdate");
    });
  }

}]);
