angular.module('application', [])
  .config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/login');

    $stateProvider
      .state('app', {
        templateUrl: "modules/application/views/layout.html",
        abstract: true
      });
  });
